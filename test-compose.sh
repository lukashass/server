#!/bin/sh

for dir in *; do
  test -d "$dir" || continue
  echo "Testing $dir ..."
  (
    cd "$dir" || exit
    if [ -f docker-compose.yml ]; then
      test -f .env || (test -f .env.dist && ln -s .env.dist .env)
      docker-compose config -q
    fi
  )
done
