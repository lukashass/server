#!/bin/sh
#
# interactive script for inspecting docker volume contents and removing the volumes afterwards
#

for volume in $(docker volume ls -q); do
  printf "\n\n"
  echo "---------------------------------------------------------------------------"
  ls --color -la /var/lib/docker/volumes/"$volume"/_data
  du -sh /var/lib/docker/volumes/"$volume"/_data
  echo "remove volume? (y/N):"
  echo "$volume"
  read -r yn
  if [ "$yn" = y ]; then
    echo "removing $volume"
    docker volume rm "$volume"
  fi
done
