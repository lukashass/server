# Server deployment

## postgres backup

See <https://github.com/prodrigestivill/docker-postgres-backup-local>

### postgres manual backup

```sh
docker-compose exec pgbackups /backup.sh
```

### postgres restore backup

1. Start only the database
1. Run below restore command
1. Start the rest of the compose stack

Replace the backupfile name, `$CONTAINER`, `$USERNAME` and `$DBNAME` from the following command (`$CONTAINER` is the database itself):

```sh
zcat backupfile.sql.gz | docker exec --tty --interactive $CONTAINER psql --username=$USERNAME --dbname=$DBNAME -W
```

## mysql backup

See <https://github.com/fradelg/docker-mysql-cron-backup>

### mysql manual backup

```sh
docker-compose exec db-backup /backup.sh
```

### mysql restore backup

1. Start only the database and the backup container
1. Run `docker-compose exec db-backup /restore.sh /backup/backupfile.sql.gz`
1. Start the rest of the compose stack
