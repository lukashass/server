#!/bin/sh
set -eu

exec busybox crond -f -l 7 -L /dev/stdout
