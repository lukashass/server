#!/bin/sh

docker-compose exec --user www-data app sh -c '
php occ config:app:set previewgenerator squareSizes --value="256" &&
php occ config:app:set previewgenerator widthSizes --value="2048"
php occ config:app:set previewgenerator heightSizes --value="2048"
php occ config:system:set preview_max_x --value 2048
php occ config:system:set preview_max_y --value 2048
php occ config:system:set jpeg_quality --value 60
php occ config:app:set preview jpeg_quality --value="60"
php occ config:system:set enabledPreviewProviders 0 --value="OC\Preview\PNG"
php occ config:system:set enabledPreviewProviders 1 --value="OC\Preview\JPEG"
php occ config:system:set enabledPreviewProviders 2 --value="OC\Preview\GIF"
php occ config:system:set enabledPreviewProviders 3 --value="OC\Preview\HEIC"
php occ config:system:set enabledPreviewProviders 4 --value="OC\Preview\BMP"
php occ config:system:set enabledPreviewProviders 5 --value="OC\Preview\XBitmap"
php occ config:system:set enabledPreviewProviders 6 --value="OC\Preview\MP3"
php occ config:system:set enabledPreviewProviders 7 --value="OC\Preview\TXT"
php occ config:system:set enabledPreviewProviders 8 --value="OC\Preview\MarkDown"
php occ config:system:set enabledPreviewProviders 9 --value="OC\Preview\OpenDocument"
php occ config:system:set enabledPreviewProviders 10 --value="OC\Preview\Krita"
php occ config:system:set enabledPreviewProviders 11 --value="OC\Preview\Movie"
php occ config:system:set enabledPreviewProviders 12 --value="OC\Preview\MKV"
php occ config:system:set enabledPreviewProviders 13 --value="OC\Preview\MP4"
php occ config:system:set enabledPreviewProviders 14 --value="OC\Preview\AVI"
'
